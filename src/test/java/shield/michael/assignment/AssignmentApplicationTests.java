package shield.michael.assignment;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.web.reactive.server.WebTestClient;
import shield.michael.assignment.model.Picture;
import shield.michael.assignment.model.PictureRepository;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = { "picture.folder=picFilesTEST" })
@AutoConfigureWebTestClient
class AssignmentApplicationTests {
	@LocalServerPort
	private int port;
	@Autowired
	private WebTestClient webClient;

	@Autowired
	private PictureRepository pictureRepository;


	static void deleteTestPicFolder() throws Exception{
		File picFolder = new File("picFilesTEST");
		File thumbnailsFolder = new File("picFilesTEST/thumbnails");
		boolean deleted = true;
		if(picFolder.exists()){

			if(thumbnailsFolder.exists()){
				Files.walk(thumbnailsFolder.toPath())
						.map(Path::toFile)
						.forEach(File::delete);
				deleted = thumbnailsFolder.delete();

			}
			Files.walk(picFolder.toPath())
					.map(Path::toFile)
					.forEach(File::delete);
			deleted = deleted && picFolder.delete();
			assertThat(deleted).isEqualTo(true);
		}
	}
	@BeforeAll
	static void init() throws Exception{
		deleteTestPicFolder();
	}

	@Test
	void savePictureTest(){
		Picture p = new Picture();
		p.setLocalPath("abc");
		p.setTitle("title1");
		p.setDownloadTime(Instant.now());
		p.setFileSizeBytes(1024l);
		p.setAlbumId(999);
		Picture savedP = pictureRepository.save(p);

		assertThat(savedP).usingRecursiveComparison().ignoringFields("id").isEqualTo(p);
	}

	@Test
	void listAllPicsTest() {
		webClient.get().uri("/listAll").exchange().expectStatus().isOk().expectBodyList(Picture.class);
	}


	@AfterAll
	static void post() throws Exception{
		deleteTestPicFolder();
	}
}
