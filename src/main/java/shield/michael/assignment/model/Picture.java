package shield.michael.assignment.model;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Picture {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column
    private Integer albumId;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String localPath;

    @Column
    private Long fileSizeBytes;

    @Column(nullable = false)
    private Instant downloadTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public Long getFileSizeBytes() {
        return fileSizeBytes;
    }

    public void setFileSizeBytes(Long fileSizeBytes) {
        this.fileSizeBytes = fileSizeBytes;
    }

    public Instant getDownloadTime() {
        return downloadTime;
    }

    public void setDownloadTime(Instant downloadTime) {
        this.downloadTime = downloadTime;
    }
}
