package shield.michael.assignment.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PictureRepository extends CrudRepository<Picture, Integer> {

    List<Picture> findByAlbumId(Integer albumId);
}
