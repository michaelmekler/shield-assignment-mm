package shield.michael.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import shield.michael.assignment.model.Picture;
import shield.michael.assignment.model.PictureRepository;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Controller
@RequestMapping
public class PictureController {

    @Autowired
    private PictureRepository pictureRepository;

    @GetMapping(path="/listAll")
    public @ResponseBody Iterable<Picture> getAllPictures() {
        return pictureRepository.findAll();
    }

    @GetMapping(path="/listByAlbum/{albumId}")
    public @ResponseBody Iterable<Picture> getAllPictures(@PathVariable Integer albumId) {
        return pictureRepository.findByAlbumId(albumId);
    }

    @GetMapping("/downloadPic/{picId}")
    public ResponseEntity<Resource> downloadPictureById(@PathVariable Integer picId, HttpServletRequest request) {
        try {
            Optional<Picture> pic = pictureRepository.findById(picId);

            if (pic.isEmpty()) return ResponseEntity.badRequest().body(null);

            Path picPath = Paths.get(pic.get().getLocalPath());
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(picPath));

            //Checks if the pic is JPEG/PNG/GIF to set the content type, if it's not then set it to raw bytes
            String contentType = request.getServletContext().getMimeType(pic.get().getLocalPath());
            contentType = contentType==null ? "application/octet-stream" : contentType;

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + picPath.getFileName().toString() + "\"")
                    .body(resource);
        }
        catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().body(null);
        }
    }
}