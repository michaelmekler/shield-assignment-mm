package shield.michael.assignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import shield.michael.assignment.model.Picture;
import shield.michael.assignment.model.PictureRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
public class AssignmentApplication{

	@Autowired
	private Environment env;

	@Autowired
	private PictureRepository pictureRepository;

	public static void main(String[] args) {
		SpringApplication.run(AssignmentApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		final RestTemplate restTemplate = new RestTemplate();

		// Needed because the external source is JSON with a content type of plain text.
		// This allows us to convert whatever is in there to JSON regardless of content type
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
		messageConverters.add(converter);
		restTemplate.setMessageConverters(messageConverters);

		return restTemplate;
	}

	// Runs when the Spring application starts up
	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			String jsonUrl = env.getProperty("picture.json.url");
			String picFolderPath = env.getProperty("picture.folder");

			try {
				PictureJSON[] picturesJSON = restTemplate.getForObject(jsonUrl, PictureJSON[].class);

				// Create the directories for the local file storage
				new File(picFolderPath).mkdir();
				new File(picFolderPath + "/thumbnails").mkdir();

				List<Picture> pictures = new ArrayList<>(picturesJSON.length);

				for (PictureJSON pictureJSON : picturesJSON) {
					Picture p = new Picture();
					p.setAlbumId(pictureJSON.getAlbumId());
					p.setTitle(pictureJSON.getTitle());

					// Downloads the picture as well as the thumbnail (the thumbnail is not used)
					File picFile = downloadPicFromURL(pictureJSON.getUrl(),pictureJSON.getTitle(),picFolderPath);
					downloadPicFromURL(pictureJSON.getThumbnailUrl(),pictureJSON.getTitle(),picFolderPath + "/thumbnails");

					p.setLocalPath(picFile.getAbsolutePath());
					p.setFileSizeBytes(picFile.length());
					p.setDownloadTime(Instant.now());
					pictures.add(p);
				}

				pictureRepository.saveAll(pictures);
			}
			catch (Exception e){
				throw new Exception("Error while getting pictures from external source", e);
			}
		};
	}

	private File downloadPicFromURL(String url,String picTitle,String directory) throws Exception{
		String extension = url.substring(url.lastIndexOf("."));
		File picFile = new File(directory + "/" + picTitle + extension);

		ReadableByteChannel readPicChannel = Channels.newChannel(new URL(url).openStream());
		FileOutputStream fileOutputStream = new FileOutputStream(picFile);
		fileOutputStream.getChannel().transferFrom(readPicChannel, 0, Long.MAX_VALUE);

		if (!picFile.exists() || picFile.length() < 1)
			throw new Exception("Problem with downloading picture from " + url);

		return picFile;
	}
}