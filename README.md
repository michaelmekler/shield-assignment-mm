# Shield Assignment MM

Uses MySQL, the DB setup is:

- `create database assignment_db;`
- `create user 'assignment'@'%' identified by 'assignmentpassword';`
- `grant all on assignment_db.* to 'assignment'@'%';`

The application.properties file sets the DB connection properties so you may change the DB name or user there, and it also sets:
- The URL of the of JSON file that populates the picture repository
- The name of the local folder where the pictures will download to


The class "AssignmentApplication" has the main function which starts the Spring application.
The server is accessed using the default port 8080. The API is:
- localhost:8080/listAll - returns a JSON list of the pictures saved to the server
- localhost:8080/listByAlbum/(albumId) - same as "listAll" except you filter by the album ID e.g /listByAlbum/1
- localhost:8080/downloadPic/(picId) - downloads a picture based on the picture ID

Requesting to download a picture that is not present should result in a Bad Request
